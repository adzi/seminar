package hr.fesb.fadzic;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
//private  WebView MyWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //MyWebView = (WebView) findViewById(R.id.webview);
        // WebSettings webSettings = MyWebView.getSettings();
        // MyWebView.loadUrl("https://www.klix.ba/");
        // webSettings.setJavaScriptEnabled(true);

        //  getWebsite();
        DataStorageGenre.fillData();
        final ListView genre_list = findViewById(R.id.list_genre);
        genre_list.setAdapter(new GenreAdapter(getApplicationContext()));


        genre_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String value = Integer.toString(position);

                Intent myIntent = new Intent(MainActivity.this, PerformersList.class);
                myIntent.putExtra("a", value);

                startActivity(myIntent);
            }
        });
        Button myevents = findViewById(R.id.myevents_button);
        myevents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myItent = new Intent(MainActivity.this, MyEvents.class);
                startActivity(myItent);

            }
        });


    }
}



   /* public static StringBuffer genre = new StringBuffer();
    public static StringBuffer genre_links=new StringBuffer();



//provjera i dohvatanje podataka preko novog threda

    public void getWebsite() {
        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    Document doc = Jsoup.connect("https://guides.ticketmaster.co.uk/concerts-and-tours-guide/").get();
                    String title = doc.title();
                    Elements genres = doc.select("*div.city__text-wrap>h4");
                    Elements links_genres=doc.select("*a.city");
                    //Log.i("fico", links_genres.toString());
                   // Log.i("fico", genres.toString());
                    int Size = genres.size();int Size2=links_genres.size();


                     int i;

                    for (i=0;i<Size;i++) {

                            Elements link = genres.eq(i);
                            genre.append(link.attr("h4")).append(link.text()).append("\n");


                        final ListView genre_list=findViewById(R.id.list_genre);


                    }
                    for (i=0;i<Size2;i++) {
                        Elements link = links_genres.eq(i);
                        genre_links.append(link.attr("href")).append("\n");




                    }
                } catch (IOException e) {
                    genre.append("Error : ").append(e.getMessage()).append("\n");
                    genre_links.append("Error : ").append(e.getMessage()).append("\n");
                    DataStorageGenre.fillData();
                    getWebsite();






                }
//vracanje na main thread
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ListView genrelist = (ListView) findViewById(R.id.list_genre);

                        DataStorageGenre.fillData();
                        final ListView genre_list = findViewById(R.id.list_genre);
                        genre_list.setAdapter(new GenreAdapter(getApplicationContext()));


                        genrelist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                String value=Integer.toString (position);

                                Intent myIntent =new Intent(MainActivity.this, PerformersList.class);
                                myIntent.putExtra("a",value);

                                startActivity(myIntent); }});




                    }

                });
            }
        }).start();}}





    /*}

    public class MyWebClient extends WebViewClient {
       @Override
       public void onPageStarted(WebView view, String url, Bitmap favicon){
          super.onPageStarted(view,url,favicon);
        }
        @Override
      public boolean shouldOverrideUrlLoading(WebView view,String url){
            view.loadUrl(url);
            return true;
        }}
        @Override
        public void onBackPressed () {
            if (MyWebView.canGoBack()) {
                MyWebView.goBack();
            } else {
                super.onBackPressed();
            }
        }



    }
*/