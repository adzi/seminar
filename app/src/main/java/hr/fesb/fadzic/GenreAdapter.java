package hr.fesb.fadzic;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import static hr.fesb.fadzic.R.layout.genre_list_first_look_item;


public class GenreAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mInflater;

    public GenreAdapter(Context context) {
        mContext = context;
        notifyDataSetChanged();

        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return DataStorageGenre.listViewData.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (view == null)
            view = mInflater.inflate(genre_list_first_look_item, parent, false);

        final TextView list_item = view.findViewById(R.id.genre_first_look);
//        final Genre Agenre= DataStorageGenre.listViewData.get(position);
        //   list_item.setText(Agenre.getGenre());
        list_item.setText(DataStorageGenre.listViewData.get(position));
        // DataStorageGenre.clearData(position);
        // Log.i("ja",String.valueOf(position));


        return view;


    }
}


