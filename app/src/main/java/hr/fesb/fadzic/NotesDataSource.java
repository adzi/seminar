package hr.fesb.fadzic;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.SQLException;
import java.util.ArrayList;

public class NotesDataSource {
    private SQLiteDatabase mDatabase;
    private MyEventsDBHelper dbHelper;

    public NotesDataSource(Context context) {
        dbHelper = new MyEventsDBHelper(context);

        try {
            this.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void open() throws SQLException {
        mDatabase = dbHelper.getWritableDatabase();
    }

    public int CheckEventId(int a) {
        Cursor cursor = mDatabase.rawQuery("SELECT * FROM MyEvents WHERE eventnumber= '" + a + "'", null);
        ArrayList<Integer> b = new ArrayList<Integer>();

        b.add(0, 0);
        cursor.moveToFirst();
        if (!cursor.isAfterLast()) {
            int c = cursor.getInt(5);
            if (c == a) {
                b.add(0, 1);
            }

        }
        return b.get(0);
    }

    public Note getNoteById(int id) {
        Note noteToReturn = new Note();

        Cursor cursor = mDatabase.rawQuery("SELECT * FROM MyEvents WHERE id = '" + id + "'", null);

        cursor.moveToFirst();

        if (!cursor.isAfterLast()) {
            noteToReturn.setID(cursor.getInt(0));
            noteToReturn.setPerformer(cursor.getString(1));
            noteToReturn.setStartdate(cursor.getString(2));
            noteToReturn.setEnddate(cursor.getString(3));
            noteToReturn.setLocation(cursor.getString(4));
            noteToReturn.setEventnumber(cursor.getInt(5));

        }

        return noteToReturn;
    }

    public ArrayList<Note> getAllNotes() {
        ArrayList<Note> notes = new ArrayList<Note>();

        Cursor cursor = mDatabase.query(MyEventsColumnNames.MyEventsEntry.TABLE_NAME, null, null, null, null, null, null);//= mDatabase.rawQuery("SELECT *	FROM  myevents", null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            Note note = new Note();
            note.setID(cursor.getInt(0));
            note.setPerformer(cursor.getString(1));
            note.setStartdate(cursor.getString(2));
            note.setEnddate(cursor.getString(3));
            note.setLocation(cursor.getString(4));
            note.setEventnumber(cursor.getInt(5));

            notes.add(note);
            cursor.moveToNext();
        }

        cursor.close();
        return notes;
    }
}
