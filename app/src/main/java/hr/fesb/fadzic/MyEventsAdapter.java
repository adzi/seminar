package hr.fesb.fadzic;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static hr.fesb.fadzic.R.layout.myeventsadapter_firstlook;

public class MyEventsAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater mInflater;

    public MyEventsAdapter(Context context) {
        mContext = context;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return DataStorageMyEvents.allNotesList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View view, final ViewGroup parent) {
        MyEventsDBHelper dbHelper = new MyEventsDBHelper(mContext);
        final SQLiteDatabase mDatabase = dbHelper.getWritableDatabase();
        if (view == null) view = mInflater.inflate(myeventsadapter_firstlook, parent, false);
        TextView performer = view.findViewById(R.id.performername);
        TextView datestart = view.findViewById(R.id.datestart);
        TextView location = view.findViewById(R.id.location);
        TextView dateend = view.findViewById(R.id.dateend);
        Note note = DataStorageMyEvents.allNotesList.get(position);
        datestart.setText(note.getStartdate());
        performer.setText(note.getPerformer());
        location.setText(note.getLocation());
        dateend.setText(note.getEnddate());
        Date datenow = new Date();
        Date datefrombase = new Date();

        Button removefrombase = view.findViewById(R.id.remove);
        final View finalView = view;
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());
        String datecompare = sdf.format(new Date());
        if (note.getStartdate().contains(",")) {

            int dot = note.getStartdate().indexOf(",");
            String date = note.getStartdate().substring(dot + 2);
            //Log.i("fico","bazadate"+date);

            // Log.i("fico","grtttttt"+datecompare);


            try {
                datenow = sdf.parse(datecompare);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            try {
                datefrombase = sdf.parse(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }


            String g = "'" + note.getStartdate() + "'";
            if (datenow.after(datefrombase)) {
                mDatabase.delete(MyEventsColumnNames.MyEventsEntry.TABLE_NAME, MyEventsColumnNames.MyEventsEntry.DATE_START + "=" + g, null);


            }
        }
        if (note.getStartdate().contains("-")) {

            int dot = note.getStartdate().indexOf("-");
            String date = note.getStartdate().substring(dot + 2);
            // Log.i("fico","bazadate"+date);
            //  Log.i("fico","grtttttt"+datecompare);


            try {
                datenow = sdf.parse(datecompare);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            try {
                datefrombase = sdf.parse(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }


            String g = "'" + note.getStartdate() + "'";
            if (datenow.after(datefrombase)) {
                mDatabase.delete(MyEventsColumnNames.MyEventsEntry.TABLE_NAME, MyEventsColumnNames.MyEventsEntry.DATE_START + "=" + g, null);


            }
        }


        removefrombase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mDatabase.delete(MyEventsColumnNames.MyEventsEntry.TABLE_NAME, MyEventsColumnNames.MyEventsEntry.EVENT_NUMBER + "=" + DataStorageMyEvents.allNotesList.get(position).getEventnumber(), null);
                DataStorageMyEvents.allNotesList.remove(position);
                notifyDataSetChanged();


            }
        });
        return view;
    }
}


