package hr.fesb.fadzic;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

public class AddToBase extends AppCompatActivity {


    private SQLiteDatabase mDatabase;
    private final ArrayList<Integer> postion = new ArrayList<>();

    @Override

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_to_base);
        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            String test = extras.getString("position");
            postion.add(0, Integer.valueOf(test));
        }


        Events events = DataStorageEvents.eventslistdata.get(postion.get(0));
        MyEventsDBHelper dbHelper = new MyEventsDBHelper(this);
        mDatabase = dbHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(MyEventsColumnNames.MyEventsEntry.PERFORMER_NAME, EventsList.bandname.get(0));
        contentValues.put(MyEventsColumnNames.MyEventsEntry.DATE_START, events.getDate());
        contentValues.put(MyEventsColumnNames.MyEventsEntry.DATE_END, events.getDate());
        contentValues.put(MyEventsColumnNames.MyEventsEntry.LOCATION, events.getLocation());
        contentValues.put(MyEventsColumnNames.MyEventsEntry.EVENT_NUMBER, events.getBaseID());
        mDatabase.insert(MyEventsColumnNames.MyEventsEntry.TABLE_NAME, null, contentValues);


        finish();


    }
}
