package hr.fesb.fadzic;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.koushikdutta.ion.Ion;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

public class EventsList extends AppCompatActivity {
    public EventsList() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.events);
        getWebsite();
    }


    final public static ArrayList<String> bandname = new ArrayList<String>();
    final public static StringBuilder eventloc = new StringBuilder();
    final public static StringBuilder date = new StringBuilder();
    final public static StringBuilder ID = new StringBuilder();
    final public static StringBuffer bend_image = new StringBuffer();

    private void getWebsite() {
        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    Bundle extras = getIntent().getExtras();
                    if (extras != null) {
                        String link = (extras.getString("a"));

                        extras.clear();


                        String Url = "https://www.viagogo.com" + link;


                        Document doc1 = Jsoup.connect(Url).get();


                        Elements location = doc1.select("*div.el-column-info.uum.ins>div>div.t.s");
                        Elements date_element = doc1.select("span.h.m.s-l");
                        Elements ID_element = doc1.select("span.camo.bk");
                        if (ID_element.isEmpty()) {
                            ID_element = doc1.select("a.camo.bk");
                        }


                        Elements image = doc1.select("div.gCol12.imgMax.el-top-image.imageHeight.oflowhide.rel>img");
                        bend_image.append(image.attr("src"));
                        int Size = location.size();
                        int Size2 = date_element.size();
                        int Size3 = ID_element.size();
                        int i = 0;
                        if (location.isEmpty()) {
                            eventloc.append("1").append("\n");
                            location.append("1").append("\n");
                        } else {
                            for (i = 0; i < Size; i++) {


                                Elements link1 = location.eq(i);

                                eventloc.append(link1.text()).append("\n");


                            }
                        }

                        if (date_element.isEmpty()) {
                            date.append("prazno").append("\n");
                            date_element.append("prazno").append("\n");
                        } else {
                            for (i = 0; i < Size2; i++) {


                                Elements link2 = date_element.eq(i);
                                date.append(link2.text()).append("\n");


                            }
                        }
                        if (ID_element.isEmpty()) {
                            ID.append("1").append("\n");
                            ID_element.append("1").append("\n");
                        } else {
                            for (i = 0; i < Size3; i++) {


                                Elements link1 = ID_element.eq(i);

                                ID.append(link1.attr("href")).append("\n");
                            }

                        }

                    }

                } catch (IOException e) {
                    eventloc.append("Error : ").append(e.getMessage()).append("\n");
                    bend_image.append("Error : ").append(e.getMessage()).append("\n");
                    date.append("Error : ").append(e.getMessage()).append("\n");
                    ID.append(0);



                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        final StringBuilder h = new StringBuilder();
                        ImageView bendimage = findViewById(R.id.bendimage);
                        Ion.with(bendimage).load(bend_image.toString());
                        bend_image.delete(0, bend_image.length());

                        Bundle extras = getIntent().getExtras();
                        final Button watch = findViewById(R.id.watchyoutube);
                        if (extras != null) {
                            final String name = extras.getString("b");
                            bandname.add(0, name);
                            name.replace(" ", "+");
                            DataStorageEvents.filldata();
                            final String error=eventloc.toString();
                            ID.delete(0, ID.length());
                            eventloc.delete(0, eventloc.length());
                            date.delete(0, date.length());
                            final WebView my = findViewById(R.id.youwatch);
                            watch.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if(error.contains("Error"))return;
                                    WebSettings webSettings = my.getSettings();
                                    webSettings.setJavaScriptEnabled(true);
                                    my.loadUrl("https://www.youtube.com/results?search_query=" + name);
                                }
                            });


                        }
                        ListView eventsList = findViewById(R.id.eventslist);
                        eventsList.setAdapter(new EventsAdapter(getApplicationContext()));
                        TextView performers = findViewById(R.id.eventsloader);
                        performers.setVisibility(View.GONE);

                    }
                });
            }
        }).start();
    }
}





