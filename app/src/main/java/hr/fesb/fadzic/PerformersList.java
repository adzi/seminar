package hr.fesb.fadzic;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;


public class PerformersList extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.performer_list_layout);
        getWebsite();
    }


    final public static StringBuilder performer_name = new StringBuilder();
    final public static StringBuilder link = new StringBuilder();
    final public static StringBuilder image = new StringBuilder();
    final public static ArrayList<String> query = new ArrayList<String>();

    private void getWebsite() {
        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    Bundle extras = getIntent().getExtras();
                    if (extras != null) {
                        String genrereceived = (extras.getString("a"));
                        extras.clear();

                        int value_used = Integer.valueOf(genrereceived);
                        String see = DataStorageGenre.listViewData.get(value_used);
                        String Url = "https://www.viagogo.com/hr/Concert-Tickets/" + see;


                        Document doc1 = Jsoup.connect(Url).get();


                        Elements performer = doc1.select("*a.t");

                        //Elements links2=doc1.select("*div.event-card__image,a.event-card__btn");
                        Elements performerlink = doc1.select("*a.t");

                        int Size = performer.size();
                        int Size2 = performerlink.size();


                        for (int i = 0; i < Size; i++) {

                            Elements link1 = performer.eq(i);
                            // Log.i("fico",link1.toString());
                            link1.last();

                            performer_name.append(link1.text()).append("\n");


                        }

                        // Log.i("fico",performer_name.toString());
                        for (int i = 0; i < Size2; i++) {

                            Elements link2 = performerlink.eq(i);
                            link.append(link2.attr("href")).append("\n");
                        }

                        // Elements b=a.select("div.gCol12.imgMax.el-top-image.imageHeight.oflowhide.rel>img");
                    }

                } catch (IOException e) {
                    link.append("Error : ").append(e.getMessage()).append("\n");

                    performer_name.append("Error : ").append(e.getMessage()).append("\n");


                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        final StringBuilder h = new StringBuilder();
                        ListView performerlist = findViewById(R.id.performer_list);

                        String performer_string = performer_name.toString();
                        //Log.i("fico",performer_string);
                        String link_string = link.toString();
                        final String[] Link = link_string.split("\n");

                        //  Log.i("fico" ,"aaaaaaa"+image);
                        DataStoragePerformer.fillperformer();

                        // Log.i("fico", "size" + performer_name.toString());
                        // link.delete(0,link.length());
                        final PerformersAdapter Adapter;
                        Adapter = new PerformersAdapter(getApplicationContext());
                        performerlist.setAdapter(Adapter);
                        final String error=performer_name.toString();

                        performer_name.delete(0, performer_name.length());
                        link.delete(0, link.length());


                        //Log.i("fico","efeffefe"+query.toString());


                        performerlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                if (error.contains("Error")){return;}

                                Intent myIntent = new Intent(PerformersList.this, EventsList.class);
                                myIntent.putExtra("a", DataStoragePerformer.link_performer.get(position));
                                myIntent.putExtra("b", DataStoragePerformer.performer_list_data.get(position));
                                startActivity(myIntent);
                            }
                        });
                        TextView performers = findViewById(R.id.performerload);
                        performers.setVisibility(View.GONE);

                    }
                });
            }
        }).start();
    }


}




