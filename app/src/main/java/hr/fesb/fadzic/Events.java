package hr.fesb.fadzic;

public class Events {
    private int ID;
    private int BaseID;
    private String location;
    private String date;

    public Events(int ID, int BaseID, String location, String date) {
        this.ID = ID;
        this.BaseID = BaseID;
        this.date = date;
        this.location = location;
    }

    public int getBaseID() {
        return BaseID;
    }

    public String getLocation() {
        return location;
    }

    public String getDate() {
        return date;
    }

}
