package hr.fesb.fadzic;

import android.util.Log;

import java.util.HashMap;


public class DataStorageEvents {

    public static final HashMap<Integer, Events> eventslistdata = new HashMap<Integer, Events>();


    public static void filldata() {
        final String ID_String = EventsList.ID.toString();

        final String[] IDArray = ID_String.split("\n");

        if (eventslistdata.isEmpty()) {
            String date = EventsList.date.toString();
            String[] date_array = date.split("\n");
            for (int i = 0; i < IDArray.length; i++) {//Log.i("fico","array   "+IDArray[i]);
                if (IDArray[i].contains("E-")) {
                    int index = IDArray[i].indexOf("E-");


                    IDArray[i] = IDArray[i].substring(index + 2);


                }
            }
            String location = EventsList.eventloc.toString();
            String[] location_array = location.split("\n");

            for (int i = 0; i < date_array.length; i++) {

                Log.i("fico", "testtest" + IDArray[i] + "  " + location_array[i] + "  " + date_array[i]);

                Events aEvents = new Events(i + 1, Integer.valueOf(IDArray[i]), location_array[i], date_array[i]);
                eventslistdata.put(i, aEvents);
            }


        } else {
            eventslistdata.clear();
            filldata();
        }
    }
}
