package hr.fesb.fadzic;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import hr.fesb.fadzic.MyEventsColumnNames.MyEventsEntry;

public class MyEventsDBHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "MyEvents.db";
    public static final int DATABASE_VERSION = 1;

    public MyEventsDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        final String SQL_CREATE_MYEVENTS_TABLE = "CREATE TABLE " +
                MyEventsEntry.TABLE_NAME +
                "(" + MyEventsEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                MyEventsEntry.PERFORMER_NAME + " TEXT NOT NULL, " + MyEventsEntry.DATE_START + " TEXT NOT NULL, " +
                MyEventsEntry.DATE_END + " TEXT NOT NULL, " + MyEventsEntry.LOCATION + " TEXT NOT NULL, " + MyEventsEntry.EVENT_NUMBER + " TEXT NOT NULL" + ");";
        db.execSQL(SQL_CREATE_MYEVENTS_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + MyEventsEntry.TABLE_NAME);
        onCreate(db);
    }
}
