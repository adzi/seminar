package hr.fesb.fadzic;

import android.provider.BaseColumns;

public class MyEventsColumnNames {
    private MyEventsColumnNames() {
    }

    public static final class MyEventsEntry implements BaseColumns {
        public static final String TABLE_NAME = "myevents";
        public static final String PERFORMER_NAME = "performer";
        public static final String DATE_START = "startdate";
        public static final String DATE_END = "enddate";
        public static final String LOCATION = "location";
        public static final String EVENT_NUMBER = "eventnumber";

    }
}
