package hr.fesb.fadzic;

import android.os.Bundle;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

public class MyEvents extends AppCompatActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NotesDataSource note = new NotesDataSource(this);
        setContentView(R.layout.myevents);
        DataStorageMyEvents.allNotesList = note.getAllNotes();
        ListView listView = findViewById(R.id.myeventslist);
        listView.setAdapter(new MyEventsAdapter(getApplicationContext()));


    }
}
