package hr.fesb.fadzic;

public class Note {
    private int ID;
    private String performer;
    private String startdate;
    private String enddate;
    private String location;
    private int eventnumber;


    public int getID() {
        return ID;
    }

    public String getPerformer() {
        return performer;
    }

    public String getStartdate() {
        return startdate;
    }

    public String getEnddate() {
        return enddate;
    }

    public String getLocation() {
        return location;
    }

    public int getEventnumber() {
        return eventnumber;
    }

    public void setID(int id) {
        this.ID = id;
    }

    public void setPerformer(String performer) {
        this.performer = performer;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public void setEnddate(String enddate) {
        this.enddate = enddate;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setEventnumber(int eventnumber) {
        this.eventnumber = eventnumber;
    }

    @Override
    public String toString() {
        return this.performer;
    }


}
