package hr.fesb.fadzic;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import static hr.fesb.fadzic.R.layout.performer_list_first_look_item;

public class PerformersAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater mInflater;

    public PerformersAdapter(Context context) {
        mContext = context;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return DataStoragePerformer.performer_list_data.size();
    }


    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (view == null)
            view = mInflater.inflate(performer_list_first_look_item, parent, false);
        final TextView list_item = view.findViewById(R.id.performeritem);
        String input = DataStoragePerformer.performer_list_data.get(position);
        list_item.setText(input);
        return view;

    }
}










