package hr.fesb.fadzic;

import java.util.ArrayList;

public class DataStorageMyEvents {

    public static ArrayList<Note> allNotesList = new ArrayList<Note>();


    public static Note getNoteById(Integer id) {
        for (int i = 0; i < allNotesList.size(); i++) {
            if (allNotesList.get(i).getID() == id) {
                return allNotesList.get(i);
            }
        }

        return null;
    }
}
