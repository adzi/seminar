package hr.fesb.fadzic;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.provider.CalendarContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;

import static hr.fesb.fadzic.R.layout.events_list_first_look_item;

public class EventsAdapter extends BaseAdapter {
    private Context mContext;
    private SQLiteDatabase mDatabase;


    private LayoutInflater mInflater;

    public EventsAdapter(Context context) {
        mContext = context;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return DataStorageEvents.eventslistdata.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (view == null)
            view = mInflater.inflate(events_list_first_look_item, parent, false);
        final Events events = DataStorageEvents.eventslistdata.get(position);
        final ArrayList<Integer> eventnumbers = new ArrayList<Integer>();
        eventnumbers.add(events.getBaseID());
        TextView date = view.findViewById(R.id.date);
        TextView location = view.findViewById(R.id.location);
        date.setText(events.getDate());
        location.setText(events.getLocation());
        Button add_to_calendar = view.findViewById(R.id.Addtocalendar);
        final Button add_to_base = view.findViewById(R.id.Addtobase);


        for (int i = 0; i < eventnumbers.size(); i++) {
            NotesDataSource note = new NotesDataSource(mContext);


            //  Log.i("fico","notenote"+note.getAllNotes());
            int test = note.CheckEventId(eventnumbers.get(i));

            //   Log.i("fico","rrrrrrrr"+eventnumbers.get(i));
            //   Log.i("fico","testtesttesttest"+test);
            if (test == 1) {
                add_to_base.setText("ADDED TO BASE");
            }
            // Log.i("fico","BASeID"+DataStorageEvents.eventslistdata.get(position).getBaseID());
            // Log.i("fico","eventnumber"+DataStorageMyEvents.allNotesList.get(i).getEventnumber());

        }
        //  Log.i("fico","BASeID"+DataStorageEvents.eventslistdata.get(position).getBaseID());


        add_to_base.setOnClickListener(new View.OnClickListener() {

                                           @Override
                                           public void onClick(View v) {if (events.getDate().contains("Error")){return;}
                                               String addtest = add_to_base.getText().toString();
                                               //  Log.i("fico", "button" + addtest);
                                               if (addtest.equals("ADD TO BASE")) {
                                                   Intent myIntent = new Intent(mContext, AddToBase.class);
                                                   myIntent.putExtra("position", String.valueOf(position));
                                                   mContext.startActivity(myIntent);
                                                   add_to_base.setText("ADDED TO BASE");
                                               }


                                           }


                                       }
        );
        add_to_calendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (events.getDate().contains("Error"))return;
                Calendar calendar = Calendar.getInstance();
                if (events.getDate().contains("prazno")) {
                    Toast.makeText(mContext, "nemoguce dodati prazan event", Toast.LENGTH_LONG).show();
                    return;
                }
                if (!events.getDate().contains("-")) {


                    int start = events.getDate().indexOf(",");
                    String date = events.getDate().substring(start + 2, start + 4);

                    int end = events.getDate().indexOf(" ", start + 6);
                    String month = events.getDate().substring(start + 5, end);
                    String year = events.getDate().substring(end + 1);
                    if (month.contains("January")) month = "0";
                    if (month.contains("February")) month = "1";
                    if (month.contains("March")) month = "2";
                    if (month.contains("April")) month = "3";
                    if (month.contains("May")) month = "4";
                    if (month.contains("June")) month = "5";
                    if (month.contains("July")) month = "6";
                    if (month.contains("August")) month = "7";
                    if (month.contains("September")) month = "8";
                    if (month.contains("October")) month = "9";
                    if (month.contains("November")) month = "10";
                    if (month.contains("December")) month = "11";


                    calendar.set(Integer.valueOf(year), Integer.valueOf(month), Integer.valueOf(date));
                    long startTime = calendar.getTimeInMillis();
                    Intent intent = new Intent(Intent.ACTION_EDIT);
                    intent.setType("vnd.android.cursor.item/event");

                    intent.putExtra("allDay", false);
                    intent.putExtra("eventLocation", events.getLocation());
                    intent.putExtra("date", events.getDate());
                    intent.putExtra("description", "aaaaaaaaaaaaa");
                    intent.putExtra("title", EventsList.bandname.get(0));
                    intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, startTime);
                    mContext.startActivity(intent);
                } else {
                    final Calendar eventend = Calendar.getInstance();

                    int start = events.getDate().indexOf("-");
                    String date = events.getDate().substring(start - 3, start - 1);

                    int end = events.getDate().indexOf(" ", start + 7);
                    String month = events.getDate().substring(start + 5, end);
                    String enddate = events.getDate().substring(start + 2, start + 4);
                    String year = events.getDate().substring(end + 1);
                    if (month.contains("January")) month = "0";
                    if (month.contains("February")) month = "1";
                    if (month.contains("March")) month = "2";
                    if (month.contains("April")) month = "3";
                    if (month.contains("May")) month = "4";
                    if (month.contains("June")) month = "5";
                    if (month.contains("July")) month = "6";
                    if (month.contains("August")) month = "7";
                    if (month.contains("September")) month = "8";
                    if (month.contains("October")) month = "9";
                    if (month.contains("November")) month = "10";
                    if (month.contains("December")) month = "11";

                    eventend.set(Integer.valueOf(year), Integer.valueOf(month), Integer.valueOf(enddate));
                    calendar.set(Integer.valueOf(year), Integer.valueOf(month), Integer.valueOf(date));
                    long startTime = calendar.getTimeInMillis();
                    long endTime = eventend.getTimeInMillis();
                    Intent intent = new Intent(Intent.ACTION_EDIT);
                    intent.setType("vnd.android.cursor.item/event");

                    intent.putExtra("allDay", false);
                    intent.putExtra("eventLocation", events.getLocation());
                    intent.putExtra("date", events.getDate());
                    intent.putExtra("description", "aaaaaaaaaaaaa");
                    intent.putExtra("title", EventsList.bandname.get(0));
                    intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, startTime);
                    intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTime);
                    mContext.startActivity(intent);
                }


            }


        });


        Button share = view.findViewById(R.id.share);
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {if (events.getDate().contains("Error"))return;
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("text/plain");
                share.putExtra(Intent.EXTRA_TEXT, "Izvođač: " + EventsList.bandname.get(0) + "  Mjesto: " + DataStorageEvents.eventslistdata.get(position).getLocation() + "  Datum: " + DataStorageEvents.eventslistdata.get(position).getDate());
                share.setPackage("com.facebook.lite");
                mContext.startActivity(Intent.createChooser(share, "share"));
            }
        });

        return view;
    }
}
